var Utility;
(function (Utility) {
    var Fees;
    (function (Fees) {
        function calculateLateFees(daysLate) {
            return daysLate * 0.25;
        }
        Fees.calculateLateFees = calculateLateFees;
    })(Fees = Utility.Fees || (Utility.Fees = {}));
    function maxBookAllowed(age) {
        return age < 12 ? 3 : 10;
    }
    Utility.maxBookAllowed = maxBookAllowed;
    function privateFunc() {
        console.log('This is a private function');
    }
})(Utility || (Utility = {}));
/// <reference path='utility-functions.ts' />
var result1 = Utility.maxBookAllowed(30);
console.log(result1);
var util = Utility.Fees;
var result2 = util.calculateLateFees(10);
console.log(result2);
