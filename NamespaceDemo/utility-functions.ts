namespace Utility {
    export namespace Fees {
        export function calculateLateFees(daysLate: number) {
            return daysLate * 0.25;
        }
    }

    export function maxBookAllowed(age: number): number {
        return age < 12? 3: 10;
    }

    function privateFunc(): void {
        console.log('This is a private function');
    }
}