import {Book} from '../src/interfaces';

export class Reader{
    name: string;
    books: Book[] = [];

    take(book: Book): void {
        this.books.push(book);
    }
}