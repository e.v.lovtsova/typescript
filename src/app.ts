import { Category } from './enums';
import { Book, Librarian, Author, Logger, Magazine } from './interfaces';
import { UL, ReferenceItem, RefBook, Shelf } from '../classes';
import { BookRequiredFields, CreateCustomerFunctionType, PersonBook, UpdatedBook } from './types';
// import * from './functions';
import {getObjectProperty,
    createCustomer,
    createCustomerId,
    getBookTitlesByCategory,
    getProperty,
    logBookTitles,
    printBook,
    printRefBook,
    purge,
    logCategorySearch,
    getBooksByCategory,
    getBooksByCategoryPromise,
    getTitles,
    logSearchResult,
} from './functions';
import { Library } from '../classes/library';

showHello('greeting', 'TypeScript');

function showHello(divName: string, name: string) {
    const elt = document.getElementById(divName);
    elt.innerText = `Hello from ${name}`;
}

// ===============================================

// logBookTitles(getBookTitlesByCategory(Category.JavaScript));

// let myId: string = createCustomerId('Ann', 10);
// console.log(myId);

// let idGenerator: (name: string, id: number) => string;
// let idGenerator: typeof createCustomerId;
// idGenerator = (name: string, id: number) => `${id} - ${name}`;
// idGenerator = createCustomerId;

// console.log(idGenerator('Alex', 20));

// createCustomer('Lev', 30, 'Lviv');

// console.log(checkoutBooks('Ann', 1, 2, 3));

// console.log(getTitles(1, true));
// console.log(getTitles(false));
// console.log(getTitles('Lea Verou'));

// console.log(bookTitleTransform('Learn TypeScript'));
// console.log(bookTitleTransform(123));

const myBook: Book = {
    id: 5,
    title: 'Colors, Backgrounds and Gradients',
    author: 'Eric A. Meyer',
    available: true,
    category: Category.CSS,
    // year: 2015,
    // copies: 3
    pages: 200,
    // markDamaged: (reason: string) => console.log(`Damaged: ${reason}`),
    markDamaged(reason: string) {
        console.log(`Damaged: ${reason}`);
    },
};
printBook(myBook);
myBook.markDamaged('missing back cover');

// Task 04.02
// const logDamage: Logger = (reason: string) => console.log(`Damaged: ${reason}`);

// Task 4.03
// const favoriteAuthor: Author = {
//     name: 'Anna',
//     email: 'anna@example.com',
//     numBooksPublished: 2,
// };
// const favoriteLibrarian: Librarian = {
//     name: 'Boris',
//     email: 'boris@example.com',
//     department: 'Classical Literature',
//     assistCustomer: null,
// };

// Task 4.04
// const offer: any = {
//     book: {
//         title: 'Essential TypeScript',
//     },
// };
// console.log(offer.magazine);
// console.log(offer.magazine?.getTitle());
// console.log(offer.book?.getTitle?.());
// console.log(offer.book?.authors?.[0]);

// Task 4.05

// console.log(getProperty(myBook, 'title'));
// console.log(getProperty(myBook, 'markDamaged'));
// console.log(getProperty(myBook, 'isbn'));

// Task 5.01

// const ref = new ReferenceItem(1, 'Learn TypeScript', 2022);
// console.log(ref);
// ref.printItem();
// ref.publisher = 'abc group';
// console.log(ref.publisher);
// console.log(ref.getId());

// Task 5.02, 5.03
// const refBook: RefBook = new RefBook(1, 'Learn TypeScript', 2022, 2);
// refBook.printItem();
// refBook.printCitation();

// Task 5.04
// const favouriteLibrarian = new UL.UniversityLibrarian();
// favouriteLibrarian.name = 'Anna';
// favouriteLibrarian.assistCustomer('Boris', 'Learn TypeScript');

// Task 5.05
// const personBook: PersonBook = {
//     name: 'Anna',
//     author: 'Anna',
//     available: false,
//     category: Category.JavaScript,
//     email: 'anna@example.com',
//     id: 1,
//     title: 'Unknown'
// };

// const options: TOptions = {duration: 20};
// const options2 = setDefoultConfig(options);
// console.log(options);
// console.log(options2);
// console.log(Object.is(options, options2));

// Task 6.03, 6.04
// const refBook = new RefBook(1, 'Learn TypeScript', 2022, 2);
// printRefBook(refBook);

// const favouriteLibrarian: Librarian = new UL.UniversityLibrarian();
// printRefBook(favouriteLibrarian);

// Task 6.05
// const flag = true;

// if (flag) {
//     import('../classes')
//         .then(o => {
//             const reader = new o.Reader();
//             reader.name = 'Anna';
//             reader.take(getAllBooks()[0]);

//             console.log(reader);
//         })
//         .catch(err => console.log(err))
//         .finally(() => console.log('Complete!'));
// }

// if (flag) {
//     const o = await import('../classes');

//     const reader = new o.Reader();
//     reader.name = 'Anna';
//     reader.take(getAllBooks()[0]);

//     console.log(reader);
// }

// Task 6.06
// let library: Library = new Library();
// let library: Library = {
//     id: 1,
//     name: 'Anna',
//     address: '',
// };

// Task 7.01
// const inventory: Book[] = [
//     { id: 10, title: 'The C Programming Language', author: '???', available: true, category: Category.Software },
//     { id: 11, title: 'Code Complete', author: 'Steve McConnell', available: true, category: Category.Software },
//     { id: 12, title: '8-Bit Graphics with Cobol', author: 'A. B.', available: true, category: Category.Software },
//     { id: 13, title: 'Cool autoexec.bat Scripts!', author: 'C. D.', available: true, category: Category.Software },
// ];

// const result1 = purge(inventory);
// console.log(result1);

// const result2 = purge([1, 2, 3]);
// console.log(result2);

// Task 7.02, 7.03
// const bookShelf: Shelf<Book> = new Shelf<Book>();
// const bookShelf = new Shelf<Book>();
// inventory.forEach(book => bookShelf.add(book));
// console.log(bookShelf.getFirst().title);

// const magazines: Magazine[] = [
//     {title: 'Programming Language Monthly', publisher: 'Code Mags'},
//     {title: 'Literary Fiction Quarterly', publisher: 'College Press'},
//     {title: 'Five Points', publisher: 'GSU'}
// ];
// const magazineShelf: Shelf<Magazine> = new Shelf<Magazine>();
// magazines.forEach(mag => magazineShelf.add(mag));
// // console.log(magazineShelf.getFirst().title);
// magazineShelf.printTitle();
// console.log(magazineShelf.find('Five Points'));

// console.log(getObjectProperty(magazines[0], 'title'));
// console.log(getObjectProperty<Book, 'author' | 'title'>(inventory[1], 'author'));


// Task 7.04
// const bookRequiredFields: BookRequiredFields = {
//     author: 'Anna',
//     available: false,
//     category: Category.JavaScript,
//     id: 1,
//     markDamaged: null,
//     pages: 200,
//     title: 'Learn JS'
// };

// const updatedBook: UpdatedBook = {
//     id: 1,
//     pages: 300,
// };

// let params: Parameters<CreateCustomerFunctionType>;
// params = ['Anna', 30, 'Kyiv'];
// createCustomer(...params);

// Task 08.01, 08.02
// const favouriteLibrarian = new UL.UniversityLibrarian();
// favouriteLibrarian['a'] = 1;
// UL.UniversityLibrarian['a'] = 2;
// UL.UniversityLibrarian.prototype['a'] = 3;

// console.log(favouriteLibrarian);
// favouriteLibrarian.name = 'Anna';
// favouriteLibrarian['printLibrarian']();

// Task 08.03
// const favouriteLibrarian = new UL.UniversityLibrarian();
// console.log(favouriteLibrarian);
// favouriteLibrarian.assistFaculty = null;
// favouriteLibrarian.teachCommunity = null;

// Task 08.04
// const refBook = new RefBook(1, 'Learn TypeScript', 2022, 2);
// refBook.printItem();

// Task 08.05
// const favouriteLibrarian = new UL.UniversityLibrarian();
// console.log(favouriteLibrarian);
// favouriteLibrarian.name = 'Anna';
// favouriteLibrarian.assistCustomer('Boris', 'Learn TypeScript');

// Task 08.06
// const favouriteLibrarian = new UL.UniversityLibrarian();
// favouriteLibrarian.name = 'Anna';
// console.log(favouriteLibrarian.name);
// favouriteLibrarian.assistCustomer('Boris', 'Learn TypeScript');

// Task 08.07
// const refBook = new RefBook(1, 'Learn TypeScript', 2022, 2);
// refBook.copies = 10;
// refBook.copies = -10;
// console.log(refBook.copies);

// Task 09.01
// console.log('Begin');
// getBooksByCategory(Category.JavaScript, logCategorySearch);
// getBooksByCategory(Category.Software, logCategorySearch);
// console.log('End');

// Task 09.02
// console.log('Begin');
// getBooksByCategoryPromise(Category.JavaScript)
//     .then(titles => {
//         console.log(titles);
//         return Promise.resolve(titles.length);
//     })
//     .then(n => console.log(n))
//     .catch(reason => console.log(reason));
// getBooksByCategoryPromise(Category.Software)
//     .then(titles => console.log(titles))
//     .catch(reason => console.log(reason));
// console.log('End');

// Task 09.03
console.log('Begin');
logSearchResult(Category.JavaScript);
logSearchResult(Category.Software).catch(err => console.log(err));
console.log('End');

