/* eslint-disable no-underscore-dangle */


enum Category {
    JavaScript,
    CSS,
    HTML,
    Software
}

export {Category};